FROM openjdk:8

WORKDIR "/katatulip"

EXPOSE 8080

COPY target/katatulip-0.0.1-SNAPSHOT.jar ./

CMD ["java", "-jar", "katatulip-0.0.1-SNAPSHOT.jar"]
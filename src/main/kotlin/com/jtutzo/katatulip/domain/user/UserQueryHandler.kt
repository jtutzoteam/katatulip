package com.jtutzo.katatulip.domain.user

import org.axonframework.queryhandling.QueryHandler
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Service
import javax.inject.Inject

@Service
class UserQueryHandler @Inject constructor(private val userProjectionRepository: UserProjectionRepository) {

    companion object {
        private val logger = LoggerFactory.getLogger(UserQueryHandler::class.java)
    }

    @QueryHandler
    fun handle(query: FindAllUserProjection): Collection<UserProjection> {
        logger.info("Find all user")
        return userProjectionRepository.findAll().toSet()
    }

    @QueryHandler
    fun handle(query: FindUserProjectionById): UserProjection {
        logger.info("Find user ${query.id}")
        return userProjectionRepository.findById(query.id)
    }
}
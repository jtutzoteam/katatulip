package com.jtutzo.katatulip.domain.user

import java.util.*

interface UserProjectionRepository {
    fun findAll(): List<UserProjection>
    fun findById(id: UUID): UserProjection
    fun create(user: UserProjection)
    fun update(user: UserProjection)
}
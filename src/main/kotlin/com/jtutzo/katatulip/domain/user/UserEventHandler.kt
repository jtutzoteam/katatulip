package com.jtutzo.katatulip.domain.user

import org.axonframework.config.ProcessingGroup
import org.axonframework.eventhandling.EventHandler
import org.axonframework.messaging.responsetypes.ResponseTypes
import org.axonframework.queryhandling.QueryUpdateEmitter
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Service
import java.util.*

@Service
@ProcessingGroup("user")
class UserEventHandler(private val userProjectionRepository: UserProjectionRepository, private val queryUpdateEmitter: QueryUpdateEmitter) {

    companion object {
        private val logger = LoggerFactory.getLogger(UserEventHandler::class.java)
    }

    @EventHandler
    fun on(evt: UserCreated) {
        logger.info("Create user ${evt.username}")
        userProjectionRepository.create(UserProjection(evt.id, evt.username, evt.email))
        emitUpdateOne(evt.id)
        emitUpdateAll()
    }

    @EventHandler
    fun on(evt: UserUpdated) {
        logger.info("Update user ${evt.username}")
        userProjectionRepository.update(UserProjection(evt.id, evt.username, evt.email))
        emitUpdateOne(evt.id)
        emitUpdateAll()
    }

    private fun emitUpdateOne(id: UUID) {
        val userProjection = userProjectionRepository.findById(id)
        val filter: (query: FindUserProjectionById) -> Boolean = { it.id == id }
        queryUpdateEmitter.emit(FindUserProjectionById::class.java,  filter, userProjection)
    }

    private fun emitUpdateAll() {
        val userProjections = userProjectionRepository.findAll()
        val filter: (query: FindAllUserProjection) -> Boolean = { true }
        queryUpdateEmitter.emit(FindAllUserProjection::class.java,  filter, userProjections)
    }

}
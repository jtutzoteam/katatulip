package com.jtutzo.katatulip.api.user

import com.jtutzo.katatulip.domain.user.FindAllUserProjection
import com.jtutzo.katatulip.domain.user.FindUserProjectionById
import com.jtutzo.katatulip.domain.user.UserProjection
import org.axonframework.messaging.responsetypes.ResponseTypes
import org.axonframework.queryhandling.QueryGateway
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import reactor.core.publisher.Flux
import java.util.*
import javax.inject.Inject

@RestController
@RequestMapping("/users")
class UserReadResource @Inject constructor(private val queryGateway: QueryGateway) {

    @GetMapping
    fun findAll(): Collection<UserProjection> =
            queryGateway.query(FindAllUserProjection(), ResponseTypes.multipleInstancesOf(UserProjection::class.java)).join()

    @GetMapping("/{id}")
    fun findById(@PathVariable("id") id: UUID): UserProjection =
            queryGateway.query(FindUserProjectionById(id), ResponseTypes.instanceOf(UserProjection::class.java)).get()

    @GetMapping("/subscribe")
    fun subscribeFindAll(): Flux<Collection<UserProjection>> {
        val query = FindAllUserProjection()
        val type = ResponseTypes.multipleInstancesOf(UserProjection::class.java)
        val subscription = queryGateway.subscriptionQuery(query, type, type)
        return Flux.concat(subscription.initialResult(), subscription.updates())
    }

    @GetMapping("/subscribe/{id}")
    fun subscribeFindById(@PathVariable("id") id: UUID): Flux<UserProjection> {
        val findUserProjectionById = FindUserProjectionById(id)
        val type = ResponseTypes.instanceOf(UserProjection::class.java)
        val subscription = queryGateway.subscriptionQuery(findUserProjectionById, type, type)
        return Flux.concat(subscription.initialResult(), subscription.updates())
    }

}
package com.jtutzo.katatulip.api.user

import com.jtutzo.katatulip.domain.user.Email
import com.jtutzo.katatulip.domain.user.Username

data class CreateUserDto(val username: Username, val email: Email)
data class UpdateUserDto(val username: Username, val email: Email)
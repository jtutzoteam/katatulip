package com.jtutzo.katatulip

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class KatatulipApplication

fun main(args: Array<String>) {
    runApplication<KatatulipApplication>(*args)
}

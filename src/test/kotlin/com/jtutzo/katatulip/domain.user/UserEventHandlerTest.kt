package com.jtutzo.katatulip.domain.user

import com.jtutzo.katatulip.util.jeremyTutzo
import com.jtutzo.katatulip.util.toUserCreated
import com.jtutzo.katatulip.util.toUserUpdated
import org.axonframework.queryhandling.QueryUpdateEmitter
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito.*

class UserEventHandlerTest {

    private lateinit var userEventHandler: UserEventHandler

    @Mock
    private val userProjectionRepository: UserProjectionRepository = mock(UserProjectionRepository::class.java)

    @Mock
    private val queryUpdateEmitter: QueryUpdateEmitter = mock(QueryUpdateEmitter::class.java)

    @Before
    fun setUp() {
        userEventHandler = UserEventHandler(userProjectionRepository, queryUpdateEmitter)
    }

    @Test
    fun `Should create user in db`() {
        // Given
        val userToCreated = jeremyTutzo()

        // When
        userEventHandler.on(userToCreated.toUserCreated())

        // Then
        verify(userProjectionRepository, times(1)).create(userToCreated)
    }

    @Test
    fun `Should update user in db`() {
        // Given
        val userToUpdated = jeremyTutzo()

        // When
        userEventHandler.on(userToUpdated.toUserUpdated())

        // Then
        verify(userProjectionRepository, times(1)).update(userToUpdated)
    }

}
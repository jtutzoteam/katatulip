# katatulip
[![build-status](https://img.shields.io/bitbucket/pipelines/jtutzoteam/katatulip.svg)](https://bitbucket.org/jtutzoteam/katatulip/addon/pipelines/home#!/)

## Overview
This is poc for test axon framework and axon server

## Build
```bash
mvn clean install
```
or with docker
```bash
docker run --rm -v "$(pwd)":/app -v ~/.m2:/root/.m2 -w /app maven:3-jdk-8-slim mvn clean install
```

## Test
```bash
mvn test
```
or with docker
```bash
docker run --rm -v "$(pwd)":/app -v ~/.m2:/root/.m2 -w /app maven:3-jdk-8-slim mvn test
```

## Run docker image
Build image in local
```bash
docker build -t jtutzo/katatulip:dev .
```

Run docker image
```bash
docker run -ti --rm --name katatulip_dev \
    -p 8080:8080 \
    -e POSTGRES_HOST=db \
    -e AXONSERVER_HOTS=axonserver \
    -e POSTGRES_DB=katatulip \
    -e POSTGRES_USER=katatulip \
    -e POSTGRES_PASSWORD=katatulip \
    jtutzo/katatulip:dev
```


## Deploy stack on docker
Build image in local
```bash
docker build -t jtutzo/katatulip:dev .
```
Deploy stack
```bash
docker stack deploy -c docker-stack.yml katatulip 
```
deploy stack with the specific tag image
```bash
env TAG=<TAG_NAME> docker stack deploy -c docker-stack.yml katatulip 
```

## Stop and remove stack
```bash
docker stack rm katatulip 
```
